﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace schoolERP_BLL
{
    public class StudnetAttendenceDetailss
    {

        public static string saveStudentAttendence(StudentAttendenceDetails[] attendence)
        {
            sqlHelper obj = new sqlHelper();
            foreach (var emp in attendence)
            {
                string exits = obj.ExecuteScaler("select StudentId from tblStudentAttendence where StudentId=" + emp.StudentId + " and  AttendenceDate='" + emp.AttendenceDate + "' and SchoolID ='"+emp.SchoolID+"' ");

                if (exits == emp.StudentId)
                {
                    //string[] cols1 = { "AttendenceType", "AttendenceDate", "TeacherId", "ClassId", "SectionId" };
                    //object[] vals1 = { emp.AttendenceType, emp.AttendenceDate, emp.EmployeeId, emp.ClassID, emp.SectionId };
                    //obj.updateValIntoTable("tblStudentAttendence", cols1, vals1, "StudentId", emp.StudentId);

                    string constr = System.Configuration.ConfigurationManager.ConnectionStrings["default"].ConnectionString;
                    SqlConnection con = new SqlConnection(constr);
                    con.Open();
                    string query = @"update tblStudentAttendence set AttendenceType='" + emp.AttendenceType + "',AttendenceDate='"
                        + emp.AttendenceDate + "',TeacherId='" + emp.EmployeeId + "',ClassId='" + emp.ClassID + "',SectionId='" + emp.SectionId
                        + "'  where StudentId=" + emp.StudentId + " and AttendenceDate='" + emp.AttendenceDate + "' and SchoolID='"+emp.SchoolID+"'";
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
                else
                {
                    string[] cols = { "StudentId", "AttendenceType", "AttendenceDate", "TeacherId", "ClassId", "SectionId" ,"SchoolID"};
                    object[] vals = { emp.StudentId, emp.AttendenceType, emp.AttendenceDate, emp.EmployeeId, emp.ClassID, emp.SectionId,emp.SchoolID };
                    obj.insertValIntoTable("tblStudentAttendence", cols, vals);
                }
            }
            return "Attendence has been Taken Successfully Thank u!";

             
        }

    }
    public class StudentAttendenceDetails
    {
        public string Id { get; set; }
        public string ClassID { get; set; }
        public string SectionId { get; set; }
        public string StudentId { get; set; }

        public string AttendenceDate { get; set; }
        public string AttendenceType { get; set; }
        public string EmployeeId { get; set; }
        public string LeaveType { get; set; }
        public string SchoolID { get; set; }
        public string School { get; set; }
        public string InTime { get; set; }
        public string OutTime { get; set; }
    }

    public class StudentAttendenceDetailsAPP
    {
        public string Id { get; set; }
        public string ClassID { get; set; }
        public string SectionId { get; set; }
        public string StudentId { get; set; }

        public string AttendenceDate { get; set; }
        public string AttendenceType { get; set; }
        public string ClassTeacherID { get; set; }
        public string SchoolID { get; set; }
       

    }
}
