﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

 
namespace schoolERP_BLL
{
  public  class VehicleDetails
    {
        public int Id { get; set; }
        public string VehicleNumber { get; set; }
        public string TotalSeats { get; set; }
        public string AllowedSeats { get; set; }
        public string OwnershipType { get; set; }
        public string InsuranceExpire { get; set; }
        public string PolutionExpire { get; set; }

        public string InsuranceExpire1 { get; set; }
        public string PolutionExpire1 { get; set; }
        public string TrackNo { get; set; }
        public string SchoolID { get; set; }
        public string School { get; set; }

    }

    public class DriverDetailss {

        public int Id { get; set; }
        public string VehicleNumber { get; set; }
        public string Name { get; set; }
        public string PresentAddress { get; set; }
        public string PramnentAddress  { get; set; }
        public string DOB { get; set; }
        public string Phoneno { get; set; }
        public string LicenseNo { get; set; }
        public string SchoolID { get; set; }
        public string School { get; set; }
    }

    public class RouteDetails
    {

        public int Id { get; set; }
        public string VehicleNumber { get; set; }
        public string RouteCode { get; set; }
        public string StartPlace { get; set; }
        public string EndPlace { get; set; }
        public string SchoolID { get; set; }
        public string School { get; set; }
        

    }

    public class DestinationDetails
    {
        public int Id { get; set; }
        public string Route { get; set; }
        public string Pickdrop { get; set; }
        public string StopTime { get; set; }
        public string SchoolID { get; set; }
        public string School { get; set; }
    }

    public class StudentTransportAllocation {
        public int Id { get; set; }

        public string route1 { get; set; }
        public string Destination1 { get; set; }
        public string UserType1 { get; set; }
        public string Class1{ get; set; }
        public string Section1 { get; set; }
        public string StudentName { get; set; }
        public string Department1 { get; set; }
        public string EmployeeName1 { get; set; }
    }
    public class TransportAllocation
    {
        public int Id { get; set; }

        public string route { get; set; }
        public string Destination { get; set; }
        public string UserType { get; set; }
        public string Class { get; set; }
        public string Section { get; set; }
        public string StudentName { get; set; }
        public string Department { get; set; }
        public string EmployeeName { get; set; }
        public string StopTime { get; set; }
        public string Pickdrop { get; set; }
        public string Status { get; set; }
        public string Extra10 { get; set; }
        public int SchoolID { get; set; }
        public List<StudentTransportAllocation> stu { get; set; }



    }
}
