﻿using schoolERP_BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolErp.Models
{



    public class StudentAPP
    {
        public bool status { get; set; }
        public string message { get; set; }
        
        public List<StudentAttendenceDetailsAPP> data { get;set;}

    }
    public class feestructureapp
    {
        public bool status { get; set; }
        public string message { get; set; }
        public feestructure data { get; set; }
      
    }
    public class feestructure
    {
        public int StudentID { get; set; }
        public int AcademicYear { get; set; }
        public string CurrentMonth { get; set; }
        public List<feestructure1> FeeDetails { get; set; }
        public List<attendencee> attendence { get; set; }
        public string SchoolID { get; set; }
        //public int Present { get; set; }
        //public int Absent { get; set; }
        //public int leave { get; set; }
        //public string Holidays { get; set; }
        //public int TotalDays { get; set; }

    }
    public class feestructure1
    {
        
        public int TotalFeeAmount { get; set; }
        public int PaidFeeAmount { get; set; }
        public int DueAmount { get; set; }
    }
  
    public class attendencee
    {
        public int Present { get; set; }
        public int Absent { get; set; }
        public int leave { get; set; }
        public int Holidays { get; set; }
        public int TotalDays { get; set; }
       
    }

    public class Student
    {
        public int ID { get; set; }

        [Display(Name = "Academic Year")]
        public string AcademicYear { get; set; }

        [Display(Name = "Registration No.")]
        public string RegNo { get; set; }

        [Display(Name = "Joining Date")]
        public Nullable<System.DateTime> JoiningDate { get; set; }

        public Nullable<int> ClassID { get; set; }

        [Display(Name = "Class")]
        public string Class { get; set; }

        [Display(Name = "Academic Year")]
        public int SectionID { get; set; }

        [Display(Name = "Section")]
        public string Section { get; set; }

        [Display(Name = "Roll No.")]
        public string RollNo { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "DOB")]
        public Nullable<System.DateTime> DOB { get; set; }

        public Nullable<int> GenderID { get; set; }

        [Display(Name = "Gender")]
        public string Gender { get; set; }

        [Display(Name = "Blood Group")]
        public string BloodGroup { get; set; }

        [Display(Name = "Birth Place")]
        public string BirthPlace { get; set; }

        [Display(Name = "Religion")]
        public string Religion { get; set; }
        public Nullable<int> ReligionID { get; set; }

        [Display(Name = "Nationality")]
        public string Nationality { get; set; }

        [Display(Name = "Mother Tongue")]
        public string MotherTongue { get; set; }

        [Display(Name = "Category")]
        public string Category { get; set; }
        public Nullable<int> CategoryID { get; set; }

        [Display(Name = "Caste")]
        public string Caste { get; set; }
        public int CasteID { get; set; }

        [Display(Name = "Aadhaar No")]
        public string AadharNo { get; set; }

        [Display(Name = "Permanent Address")]
        public string PermanentAddress { get; set; }

        [Display(Name = "Corresponding Address")]
        public string CorrespondAddress { get; set; }

        [Display(Name = "Pincode")]
        public decimal pincode { get; set; }

        [Display(Name = "Country")]
        public string country { get; set; }
        public Nullable<int> CountryID { get; set; }

        [Display(Name = "State")]
        public string state { get; set; }
        public Nullable<int> StateID { get; set; }

        [Display(Name = "Telephone")]
        public Nullable<decimal> phone { get; set; }

        [Display(Name = "Mobile No.")]
        public string mobileNo { get; set; }

        [Display(Name = "Email")]
        public string emailID { get; set; }

        [Display(Name = "Select Image")]
        public string PicPath { get; set; }

        [Display(Name = "Name")]
        public string FatherName { get; set; }

        [Display(Name = "Mobile No.")]
        public string FMobile { get; set; }

        [Display(Name = "Job")]
        public string FJob { get; set; }

        [Display(Name = "Email")]
        public string Fmail { get; set; }

        [Display(Name = "Academic Year")]
        public string FQualification { get; set; }
        public Nullable<int> FQualificationID { get; set; }

        [Display(Name = "Aadhaar No.")]
        public string FAdharNo { get; set; }

        [Display(Name = "Name")]
        public string MotherName { get; set; }

        [Display(Name = "Mobile No.")]
        public string Mmobile { get; set; }

        [Display(Name = "Job")]
        public string MJob { get; set; }

        [Display(Name = "Email")]
        public string Mmail { get; set; }

        [Display(Name = "Academic Year")]
        public string MQualification { get; set; }
        public Nullable<int> MQualificationID { get; set; }

        [Display(Name = "Aadhaar No.")]
        public string MAdharNo { get; set; }

        [Display(Name = "Academic Year")]
        public Nullable<int> IsParentAvail { get; set; }

        [Display(Name = "Income")]
        public string FIncome { get; set; }

        [Display(Name = "Income")]
        public string MIncome { get; set; }

        [Display(Name = "Guardian Name")]
        public string GuardianName { get; set; }

        [Display(Name = "Relation")]
        public string Relation { get; set; }

        [Display(Name = "Qualification")]
        public string GQualification { get; set; }
        public Nullable<int> GQualificationID { get; set; }

        [Display(Name = "Job")]
        public string GJob { get; set; }

        [Display(Name = "Income")]
        public string GIncome { get; set; }

        [Display(Name = "Previous School")]
        public string PrevSchool { get; set; }

        [Display(Name = "Previous School Address")]
        public string PSAddress { get; set; }

        [Display(Name = "Class Passed")]
        public string ClassPassed { get; set; }

        [Display(Name = "Academic Year")]
        public string RoleNm { get; set; }

        public Nullable<int> RouteID { get; set; }

        public Nullable<int> DestinationID { get; set; }

        [Display(Name = "Destination")]
        public string DestinationNm { get; set; }

        [Display(Name = "User ID")]
        public string SUserID { get; set; }

        [Display(Name = "Password")]
        public string SPwd { get; set; }

        [Display(Name = "User ID")]
        public string PUserID { get; set; }

        [Display(Name = "Password")]
        public string PPwd { get; set; }
        public Nullable<int> Status { get; set; }

        public string StatusName { get; set; }

        public Nullable<int> PSID { get; set; }
        public string PSYear { get; set; }
        public string School { get; set; }
        public string Board { get; set; }
        public Nullable<int> PSClass { get; set; }
        public string Marks { get; set; }
        public string Awards { get; set; }
        public Nullable<int> PSStatus { get; set; }

        public Nullable<int> PSID2 { get; set; }
        public string PSYear2 { get; set; }
        public string School2 { get; set; }
        public string Board2 { get; set; }
        public Nullable<int> PSClass2 { get; set; }
        public string Marks2 { get; set; }
        public string Awards2 { get; set; }
        public Nullable<int> PSStatus2 { get; set; }

        public Nullable<int> PSID3 { get; set; }
        public string PSYear3 { get; set; }
        public string School3 { get; set; }
        public string Board3 { get; set; }
        public Nullable<int> PSClass3 { get; set; }
        public string Marks3 { get; set; }
        public string Awards3 { get; set; }
        public Nullable<int> PSStatus3 { get; set; }
        public Nullable<int> StudentID { get; set; }

        public Nullable<int> HID { get; set; }
        public Nullable<double> Height { get; set; }
        public Nullable<double> Weight { get; set; }
        public Nullable<double> VisionLeft { get; set; }
        public Nullable<double> VisionRight { get; set; }
        public string MedicationDetails { get; set; }
        public Nullable<int> HStatus { get; set; }

        public Nullable<int> docLength { get; set; }
        public TBLStudentDoc doc { get; set; }

        public string GNationality { get; set; }
        public string Gmail { get; set; }
        public string Gmobile { get; set; }
        public string GOfficeAddress { get; set; }
        public string GAdharNo { get; set; }
        public string Languages { get; set; }
        public Nullable<System.DateTime> MDOB { get; set; }
        public string MDesig { get; set; }
        public string MNationality { get; set; }
        public string MOfficeAddress { get; set; }

        public Nullable<decimal> PermPincode { get; set; }
        public string PermState { get; set; }
        public string PermStateNm { get; set; }
        public string PermCity { get; set; }
        public Nullable<decimal> CorsPincode { get; set; }
        public string CorsState { get; set; }
        public string CorsStateNm { get; set; }
        public string CorsCity { get; set; }
        //fatch Find Student for id (Edit)
        public string PermcountrynameNm1 { get; set; }
        public string croscountrynameNm1 { get; set; }
        public string PermState1  { get; set; }
         public string CorsState1  { get; set; }
        public string PermCity1  { get; set; }
        public string CorsCity1  { get; set; }

        public string SMSmobileNo { get; set; }
        public Nullable<System.DateTime> FDOB { get; set; }
        public string FDesig { get; set; }
        public string FNationality { get; set; }
        public string FOfficeAddress { get; set; }
        public string GDesig { get; set; }
        public string EmergencyNo { get; set; }
        public string EmerContPerson { get; set; }
        public string ContPersRelation { get; set; }
        public string FPicPath { get; set; }
        public string MPicPath { get; set; }

        public List<TBLPrevSchoolDet> prevlist { get; set; }

        public TBLStudentHealthDet ht { get; set; }
        public tblSchoolDetail sd { get; set; }
        public string SRNo { get; set; }
        public string TCNo { get; set; }

        public string Lang_known { get; set; }

        public string stream { get; set; }
        public Nullable<int> streamID { get; set; }

        public int batchid { get; set; }
        public string batch { get; set; }
        public string SStatus { get; set; }
        public string Extra10 { get; set; }
        public string principalSign { get; set; }
        public string SDOB { get; set; }
        public string jDate { get; set; }
        public List<StudentAttendenceDetails> attendenceList { get; set; }

        public tblStRegistration stReg { get; set; }
        public List<tblStRegPrevSchoolDet> prevlist_reg { get; set; }
        public tblStRegDoc rgDoc { get; set; }
        public tblDocument docName { get; set; }
        public List<Student> rgDoclist { get; set; }
        public tblStRegHealthDet stregHt { get; set; }
        public string SchoolID { get; set; }
        public string Paidamount { get; set; }
        public string Dueamount { get; set; }
        public string loginuser { get; set; }
        public string typeuser { get; set; }
        public string PermCityNm { get; set; }
        public string corrCityNm { get; set; }
        public string PermcountrynameNm { get; set; }
        public string croscountrynameNm { get; set; }
        public string Message { get; set; }
        public string status { get; set; }
        public Student data { get; set; }
        // public string status { get; set; }
        public int Total { get; set; }
        public string ResonForLeave { get; set; }
        public int StuID { get; set; }
        public Nullable<System.DateTime> LeaveTo { get; set; }
        public Nullable<System.DateTime> LeaveFrom { get; set; }
    }


    public class years
    {
        public string year { get; set; }
    }

    public class StudentLeaveApplyAPP
    {
        public string status { get; set; }
        public string message { get; set; }
        public List<StudentLeaveApply> data { get; set; }
    }

        public class StudentLeaveApply
    {
        public int leaveID { get; set; }
        public tblStudentLeaveApply leave { get; set; }
        public string datefrom { get; set; }
        public string dateto { get; set; }
        public string RequestDt { get; set; }
        public string statusNm { get; set; }
        public string FirstName { get; set; }
        public string MidName { get; set; }
        public string LastName { get; set; }
        public string RollNo { get; set; }

        public string style { get; set; }
        public string color { get; set; }
        public string SchoolID { get; set; }
    }

    public class studentdetails
    {
       
        public bool status { get; set; }
        public string message { get; set; }
       
        public Student data { get; set; }

    }
    public class ClassTeacherAllocation
    {
        public string Id { get; set; }
        public string ClassID { get; set; }
        public string Class { get; set; }
        public string SectionId { get; set; }
        public string section { get; set; }
        public string FirstName { get; set; }
        public string MidName { get; set; }
        public string LastName { get; set; }
        public string RollNo { get; set; }

        public string style { get; set; }
        public string color { get; set; }
    }




}