﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using schoolERP_BLL;
using SchoolErp.Models;
using System.IO;

namespace SchoolErp.Controllers.WebApi
{
    public class TransportApiController : ApiController
    {
        SCHOOLERPEntities db = new SCHOOLERPEntities();
        //[System.Web.Http.Route("api/TransportApi/saveVehicleDetails")]
        //[System.Web.Http.HttpPost]
        //public string saveVehicleDetails(VehicleDetails vd)
        //{
        //    if (vd.Id == 0)
        //    {
        //        tblTransportVehicalDetail tbl = new tblTransportVehicalDetail();
        //        tbl.VehicalNo = vd.VehicleNumber;
        //        tbl.TotalSeats = vd.TotalSeats;
        //        tbl.AllowedSeats = vd.AllowedSeats;
        //        tbl.OwnershipType = vd.OwnershipType;
        //        tbl.InsuranceDate = vd.InsuranceExpire;
        //        tbl.PollutionDate = vd.PolutionExpire;
        //        tbl.TrackNumber = vd.TrackNo;
        //        tbl.DateCreated = DateTime.Now;

        //        db.tblTransportVehicalDetails.Add(tbl);
        //        db.SaveChanges();
        //        return "Vehicle Inserted Successfully";
        //    }
        //    else
        //    {
        //        var result = db.tblTransportVehicalDetails.Where(s => s.Id == vd.Id).SingleOrDefault();
        //        result.VehicalNo = vd.VehicleNumber;
        //        result.TotalSeats = vd.TotalSeats;
        //        result.AllowedSeats = vd.AllowedSeats;
        //        result.OwnershipType = vd.OwnershipType;
        //        result.InsuranceDate = vd.InsuranceExpire;
        //        result.PollutionDate = vd.PolutionExpire;
        //        result.TrackNumber = vd.TrackNo;
        //        result.DateCreated = DateTime.Now;
        //        db.SaveChanges();
        //        return "Vehicle Updated Successfully";
        //    }


        //}




        [System.Web.Http.Route("api/TransportApi/viewAllVehicleDetailsbySchool")]
        [System.Web.Http.HttpPost]
        public VehicleDetails[] viewAllVehicleDetailsbySchool(List<string> aa)
        {
            List<VehicleDetails> list = new List<VehicleDetails>();
            sqlHelper obj = new sqlHelper();
            int SchoolID = Convert.ToInt32(aa[0]);
            DataTable dt = obj.getDataTable(@"select a.id,a.VehicalNo,a.TotalSeats,a.AllowedSeats,a.OwnershipType,a.InsuranceDate,a.PollutionDate,a.TrackNumber,a.DateCreated,a.SchoolID,B.School from tblTransportVehicalDetails a left outer join tblSchoolDetails b on b.ID=a.SchoolID where  a.SchoolID='"+SchoolID+"' and  a.IsDeleted is null");
            foreach (DataRow dr in dt.Rows)
            {
                VehicleDetails usr = new VehicleDetails();
                usr.Id = int.Parse(dr["Id"].ToString());
                usr.VehicleNumber = dr["VehicalNo"].ToString();
                usr.TotalSeats = dr["TotalSeats"].ToString();
                usr.AllowedSeats = dr["AllowedSeats"].ToString();

                usr.OwnershipType = dr["OwnershipType"].ToString();

                usr.InsuranceExpire1 = dr["InsuranceDate"].ToString();
                usr.PolutionExpire1 = dr["PollutionDate"].ToString();
                usr.TrackNo = dr["TrackNumber"].ToString();
                usr.SchoolID = dr["SchoolID"].ToString();
                usr.School = dr["School"].ToString();


                list.Add(usr);
            }
            return list.ToArray();
        }


        [System.Web.Http.Route("api/TransportApi/viewAllVehicleDetails")]
        [System.Web.Http.HttpPost]
        public VehicleDetails[] viewAllVehicleDetails()
        {
            List<VehicleDetails> list = new List<VehicleDetails>();
            sqlHelper obj = new sqlHelper();
            DataTable dt = obj.getDataTable(@"select a.id,a.VehicalNo,a.TotalSeats,a.AllowedSeats,a.OwnershipType,a.InsuranceDate,a.PollutionDate,a.TrackNumber,a.DateCreated,a.SchoolID,B.School from tblTransportVehicalDetails a left outer join tblSchoolDetails b on b.ID=a.SchoolID where a.IsDeleted is null");
            foreach (DataRow dr in dt.Rows)
            {
                VehicleDetails usr = new VehicleDetails();
                usr.Id = int.Parse(dr["Id"].ToString());
                usr.VehicleNumber = dr["VehicalNo"].ToString();
                usr.TotalSeats = dr["TotalSeats"].ToString();
                usr.AllowedSeats = dr["AllowedSeats"].ToString();

                usr.OwnershipType = dr["OwnershipType"].ToString();

                usr.InsuranceExpire1 = dr["InsuranceDate"].ToString();
                usr.PolutionExpire1 = dr["PollutionDate"].ToString();
                usr.TrackNo = dr["TrackNumber"].ToString();
                usr.SchoolID = dr["SchoolID"].ToString();
                usr.School = dr["School"].ToString();


                list.Add(usr);
            }
            return list.ToArray();
        }

        // [System.Web.Http.Route("api/TransportApi/viewAllVehicleDetails")]
        // [System.Web.Http.HttpPost]
        //  public List<VehicleDetails> viewAllVehicleDetails()
        // {




        //sqlHelper obj = new sqlHelper();
        //List<VehicleDetails> list = new List<VehicleDetails>();

        //var vehicle = db.tblTransportVehicalDetails.ToList();
        //foreach (var a in vehicle)
        //{
        //    VehicleDetails usr = new VehicleDetails();
        //    usr.Id = a.Id;
        //    usr.VehicleNumber = a.VehicalNo;
        //    usr.TotalSeats = a.TotalSeats;
        //    usr.AllowedSeats = a.AllowedSeats;
        //    usr.OwnershipType = a.OwnershipType;
        //    usr.InsuranceExpire1 = a.InsuranceDate;
        //    usr.PolutionExpire1 = a.PollutionDate;
        //    usr.TrackNo = a.TrackNumber;
        //    usr.SchoolID = a.SchoolID;
        //    list.Add(usr);


        //  }
        //
        //  return list;
        //  }
        [System.Web.Http.Route("api/TransportApi/editVehicleById")]
        [System.Web.Http.HttpPost]
        public VehicleDetails editVehicleById(List<string> id)
        {
            int idd = Convert.ToInt16(id[0]);
            sqlHelper obj = new sqlHelper();
            DataTable dt = obj.getDataTable(@"select * from tblTransportVehicalDetails where Id='" + idd + "'");
            VehicleDetails usr = new VehicleDetails();
            foreach (DataRow dr in dt.Rows)
            {

                usr.Id = int.Parse(dr["Id"].ToString());
                usr.VehicleNumber = dr["VehicalNo"].ToString();
                usr.TotalSeats = dr["TotalSeats"].ToString();
                usr.AllowedSeats = dr["AllowedSeats"].ToString();

                usr.OwnershipType = dr["OwnershipType"].ToString();

                usr.InsuranceExpire1 = dr["InsuranceDate"].ToString();
                usr.PolutionExpire1 = dr["PollutionDate"].ToString();
                usr.TrackNo = dr["TrackNumber"].ToString();
                usr.SchoolID = dr["SchoolID"].ToString();
                // usr.School = dr["School"].ToString();



            }
            return usr;
        }
        //[System.Web.Http.Route("api/TransportApi/deleteVehicleById")]
        //[System.Web.Http.HttpPost]
        //public string deleteVehicleById(string Id)
        //{
        //    bool b = LeaveManagement.deleteVehicleById(Id);
        //    if (b)
        //    {
        //        return "Vehicle Details  Deleted Successfully";
        //    }
        //    else
        //    {
        //        return "Vehicle Details not Deleted ";
        //    }

        //}

        //[System.Web.Http.Route("api/TransportApi/editVehicleById")]
        //[System.Web.Http.HttpPost]
        //public VehicleDetails editVehicleById(List<string> id)
        //{
        //    int idd = Convert.ToInt16(id[0]);
        //    var result = db.tblTransportVehicalDetails.Where(s => s.Id == idd).SingleOrDefault();
        //    VehicleDetails usr = new VehicleDetails();
        //    if (result != null)
        //    {
        //        usr.Id = result.Id;
        //        usr.VehicleNumber = result.VehicalNo;
        //        usr.TotalSeats = result.TotalSeats;
        //        usr.AllowedSeats = result.AllowedSeats;
        //        usr.OwnershipType = result.OwnershipType;
        //        usr.InsuranceExpire1 = result.InsuranceDate;
        //        usr.PolutionExpire1 = result.PollutionDate;
        //        usr.TrackNo = result.TrackNumber;
        //        usr.SchoolID = result.SchoolID;
        //    }
        //    return usr;

        //}


        [System.Web.Http.Route("api/TransportApi/getAllVehiclebySchool")]
        [System.Web.Http.HttpPost]
        public DepartmentMaster[] getAllVehiclebySchool(List<string> aa)
        {
            int SchoolID = Convert.ToInt32(aa[0]);
            List<DepartmentMaster> list = new List<DepartmentMaster>();
            sqlHelper obj = new sqlHelper();
            DataTable dt = obj.getDataTable(@"select Id,VehicalNo from tblTransportVehicalDetails where SchoolID='"+ SchoolID + "' and IsDeleted is null");
            foreach (DataRow dr in dt.Rows)
            {
                DepartmentMaster usr = new DepartmentMaster();
                usr.Id = dr["Id"].ToString();
                usr.Name = dr["VehicalNo"].ToString();

                list.Add(usr);
            }
            return list.ToArray();


        }



        [System.Web.Http.Route("api/TransportApi/getAllVehicle")]
        [System.Web.Http.HttpPost]
        public DepartmentMaster[] getAllVehicle()
        {
            List<DepartmentMaster> list = new List<DepartmentMaster>();
            sqlHelper obj = new sqlHelper();
            DataTable dt = obj.getDataTable(@"select Id,VehicalNo from tblTransportVehicalDetails");
            foreach (DataRow dr in dt.Rows)
            {
                DepartmentMaster usr = new DepartmentMaster();
                usr.Id = dr["Id"].ToString();
                usr.Name = dr["VehicalNo"].ToString();

                list.Add(usr);
            }
            return list.ToArray();


        }


        [System.Web.Http.Route("api/TransportApi/fetchAllDetailsDriverByDriverId")]
        [System.Web.Http.HttpPost]
        public EmployeeEm fetchAllDetailsDriverByDriverId(List<string> val)
        {

            int idd = Convert.ToInt32(val[0]);
            var result = db.tblEmployees.SingleOrDefault(s => s.Id == idd);
            EmployeeEm usr = new EmployeeEm();
            usr.PresentAddress = result.PresentAddress;
            usr.PermanentAddress = result.PermAddress;
            usr.Mobile = result.Mobile;
            usr.DOB = result.DOB.ToString();


            return usr;
        }

        [System.Web.Http.Route("api/TransportApi/saveDriverDetails")]
        [System.Web.Http.HttpPost]
        public string saveDriverDetails(DriverDetailss vd)
        {
            string b = LeaveManagement.saveDriverDetails(vd);
            if (b != "")
            {
                return b;
            }
            else
            {
                return "";
            }

        }

        //[System.Web.Http.Route("api/TransportApi/saveDriverDetails")]
        //[System.Web.Http.HttpPost]
        //public string saveDriverDetails(DriverDetails vd)
        //{
        //    if (vd.Id == 0)
        //    {
        //        tblTransportDriver tbl = new tblTransportDriver();
        //        tbl.VehicleId = Convert.ToInt16(vd.VehicleNumber);
        //        tbl.DriveId = Convert.ToInt16(vd.Name);
        //        tbl.PresentAddress = vd.PresentAddress;
        //        tbl.PermanentAddress = vd.PramnentAddress;
        //        tbl.DOB = Convert.ToDateTime(vd.DOB);
        //        tbl.phone = vd.Phoneno;
        //        tbl.LicenseNo = vd.LicenseNo;
        //        tbl.DateCreated = DateTime.Now;

        //        db.tblTransportDrivers.Add(tbl);
        //        db.SaveChanges();
        //        return "Driver Inserted Successfully";
        //    }
        //    else
        //    {
        //        var result = db.tblTransportDrivers.Where(s => s.Id == vd.Id).SingleOrDefault();
        //        result.VehicleId = Convert.ToInt16(vd.VehicleNumber);
        //        result.DriveId = Convert.ToInt16(vd.Name);
        //        result.PresentAddress = vd.PresentAddress;
        //        result.PermanentAddress = vd.PramnentAddress;
        //        result.DOB = Convert.ToDateTime(vd.DOB);
        //        result.phone = vd.Phoneno;
        //        result.LicenseNo = vd.LicenseNo;
        //        result.DateCreated = DateTime.Now;

        //        db.SaveChanges();
        //        return "Driver Updated Successfully";
        //    }


        //}



        [System.Web.Http.Route("api/TransportApi/viewAllDriverDetailsDetailssBySchool")]
        [System.Web.Http.HttpPost]
        public DriverDetailss[] viewAllDriverDetailsDetailssBySchool(List<string> aa)
        {

            int SchoolID = Convert.ToInt32(aa[0]);
            List<DriverDetailss> list = new List<DriverDetailss>();
            sqlHelper obj = new sqlHelper();
            DataTable dt = obj.getDataTable(@"select a.school,d.SchoolID, d.Id,v.VehicalNo,name= e.FirstName + e.MiddleName + e.LastName,d.PermanentAddress,d.PresentAddress,d.DOB,d.phone  from tblTransportDriver d join  tblEmployee e on d.DriveId = e.Id join  tblTransportVehicalDetails v on d.VehicleId = v.Id left outer join tblSchoolDetails a on d.SchoolID=a.ID where d.SchoolID='"+ SchoolID + "' and  v.IsDeleted is null");
            foreach (DataRow dr in dt.Rows)
            {


                DriverDetailss usr = new DriverDetailss();
                usr.Id = int.Parse(dr["Id"].ToString());
                usr.VehicleNumber = dr["VehicalNo"].ToString();
                usr.Name = dr["name"].ToString();
                usr.PramnentAddress = dr["PermanentAddress"].ToString();
                usr.PresentAddress = dr["PresentAddress"].ToString();

                usr.DOB = dr["DOB"].ToString();

                usr.Phoneno = dr["phone"].ToString();
                usr.LicenseNo = dr["VehicalNo"].ToString();
                usr.SchoolID = dr["SchoolID"].ToString();
                usr.School = dr["School"].ToString();


                list.Add(usr);
            }
            return list.ToArray();
        }






        [System.Web.Http.Route("api/TransportApi/viewAllDriverDetailsDetailss")]
        [System.Web.Http.HttpPost]
        public DriverDetailss[] viewAllDriverDetailsDetailss()
        {
            List<DriverDetailss> list = new List<DriverDetailss>();
            sqlHelper obj = new sqlHelper();
            DataTable dt = obj.getDataTable(@"select a.school,d.SchoolID, d.Id,v.VehicalNo,name= e.FirstName + e.MiddleName + e.LastName,d.PermanentAddress,d.PresentAddress,d.DOB,d.phone  from tblTransportDriver d join  tblEmployee e on d.DriveId = e.Id join  tblTransportVehicalDetails v on d.VehicleId = v.Id left outer join tblSchoolDetails a on d.SchoolID=a.ID");
            foreach (DataRow dr in dt.Rows)
            {


                DriverDetailss usr = new DriverDetailss();
                usr.Id = int.Parse(dr["Id"].ToString());
                usr.VehicleNumber = dr["VehicalNo"].ToString();
                usr.Name = dr["name"].ToString();
                usr.PramnentAddress = dr["PermanentAddress"].ToString();
                usr.PresentAddress = dr["PresentAddress"].ToString();

                usr.DOB = dr["DOB"].ToString();

                usr.Phoneno = dr["phone"].ToString();
                usr.LicenseNo = dr["VehicalNo"].ToString();
                usr.SchoolID = dr["SchoolID"].ToString();
                usr.School = dr["School"].ToString();


                list.Add(usr);
            }
            return list.ToArray();
        }


        //[System.Web.Http.Route("api/TransportApi/viewAllDriverDetailsDetails")]
        //[System.Web.Http.HttpPost]
        //public List<DriverDetails> viewAllDriverDetailsDetails()
        //{
        //    sqlHelper obj = new sqlHelper();
        //    List<DriverDetails> list = new List<DriverDetails>();

            //    var vehicle = (from d in db.tblTransportDrivers
            //                   join e in db.tblEmployees on d.DriveId equals e.Id
            //                   join v in db.tblTransportVehicalDetails on d.VehicleId equals v.Id
            //                   select new { d, Name = e.FirstName + " " + e.MiddleName + " " + e.LastName, v.VehicalNo }).ToList();
            //    foreach (var a in vehicle)
            //    {
            //        DriverDetails usr = new DriverDetails();
            //        usr.Id = a.d.Id;
            //        usr.VehicleNumber = a.VehicalNo;
            //        usr.Name = a.Name;
            //        usr.PramnentAddress = a.d.PermanentAddress;
            //        usr.PresentAddress = a.d.PresentAddress;
            //        usr.DOB = Convert.ToString(a.d.DOB);
            //        usr.Phoneno = a.d.phone;
            //        list.Add(usr);
            //    }



            //    return list;
            //}


            //[System.Web.Http.Route("api/TransportApi/saveRouteDetails")]
            //[System.Web.Http.HttpPost]
            //public string saveRouteDetails(RouteDetails vd)
            //{
            //    if (vd.Id == 0)
            //    {
            //        tblTransRoute tbl = new tblTransRoute();
            //        tbl.VehicleId = Convert.ToInt16(vd.VehicleNumber);

            //        tbl.RouteCode = vd.RouteCode;
            //        tbl.StartPlace = vd.StartPlace;
            //        tbl.EndPlace = vd.EndPlace;

            //        tbl.DateCreated = DateTime.Now;

            //        db.tblTransRoutes.Add(tbl);
            //        db.SaveChanges();
            //        return "Route Inserted Successfully";
            //    }
            //    else
            //    {
            //        var result = db.tblTransRoutes.Where(s => s.Id == vd.Id).SingleOrDefault();
            //        result.VehicleId = Convert.ToInt16(vd.VehicleNumber);
            //        result.RouteCode = vd.RouteCode;
            //        result.StartPlace = vd.StartPlace;
            //        result.EndPlace = vd.EndPlace;
            //        result.DateCreated = DateTime.Now;
            //        db.SaveChanges();
            //        return "Route Updated Successfully";
            //    }


            //}


            //[System.Web.Http.Route("api/TransportApi/viewAllRouteDetails")]
            //[System.Web.Http.HttpPost]
            //public List<RouteDetails> viewAllRouteDetails()
            //{
            //    sqlHelper obj = new sqlHelper();
            //    List<RouteDetails> list = new List<RouteDetails>();

            //    var vehicle = (from d in db.tblTransRoutes
            //                   join v in db.tblTransportVehicalDetails on d.VehicleId equals v.Id
            //                   select new { d, v.VehicalNo }).ToList();
            //    foreach (var a in vehicle)
            //    {
            //        RouteDetails usr = new RouteDetails();
            //        usr.Id = a.d.Id;
            //        usr.VehicleNumber = a.VehicalNo;
            //        usr.RouteCode = a.d.RouteCode;
            //        usr.StartPlace = a.d.StartPlace;
            //        usr.EndPlace = a.d.EndPlace;

            //        list.Add(usr);
            //    }



            //    return list;
            //}

        [System.Web.Http.Route("api/TransportApi/viewAllRouteDetails")]
        [System.Web.Http.HttpPost]
        public RouteDetails[] viewAllRouteDetails()
        {
            List<RouteDetails> list = new List<RouteDetails>();
            sqlHelper obj = new sqlHelper();
            DataTable dt = obj.getDataTable(@"select d.Id,v.VehicalNo,d.RouteCode,d.StartPlace,d.EndPlace,a.School,d.SchoolID from tblTransRoute  d join  tblTransportVehicalDetails v on d.VehicleId= v.Id 
left outer join tblSchoolDetails a on a.ID=d.SchoolID
 where d.IsDeleted is null");
            foreach (DataRow dr in dt.Rows)
            {
                RouteDetails usr = new RouteDetails();
                usr.Id = int.Parse(dr["Id"].ToString());
                usr.VehicleNumber = dr["VehicalNo"].ToString();
                usr.RouteCode = dr["RouteCode"].ToString();
                usr.StartPlace = dr["StartPlace"].ToString();

                usr.EndPlace = dr["EndPlace"].ToString();

               
                usr.SchoolID = dr["SchoolID"].ToString();
                usr.School = dr["School"].ToString();


                list.Add(usr);
            }
            return list.ToArray();
        }



        [System.Web.Http.Route("api/TransportApi/editRouteById")]
        [System.Web.Http.HttpPost]
        public RouteDetails editRouteById(List<string> id)
        {
            int idd = Convert.ToInt16(id[0]);
            sqlHelper obj = new sqlHelper();
            DataTable dt = obj.getDataTable(@"select * from tblTransRoute where Id='" + idd + "'");
            RouteDetails usr = new RouteDetails();
            foreach (DataRow dr in dt.Rows)
            {

                usr.Id = int.Parse(dr["Id"].ToString());
                usr.VehicleNumber = dr["VehicleId"].ToString();
                usr.RouteCode = dr["RouteCode"].ToString();
                usr.StartPlace = dr["StartPlace"].ToString();
                usr.EndPlace = dr["EndPlace"].ToString();

                usr.SchoolID = dr["SchoolID"].ToString();
            }
            return usr;
        }
        //[System.Web.Http.Route("api/TransportApi/editRouteById")]
        //[System.Web.Http.HttpPost]
        //public RouteDetails editRouteById(List<string> id)
        //{
        //    int idd = Convert.ToInt16(id[0]);
        //    var result = db.tblTransRoutes.Where(s => s.Id == idd).SingleOrDefault();
        //    RouteDetails usr = new RouteDetails();
        //    if (result != null)
        //    {
        //        usr.Id = result.Id;
        //        usr.VehicleNumber = Convert.ToString(result.VehicleId);
        //        usr.RouteCode = result.RouteCode;
        //        usr.StartPlace = result.StartPlace;
        //        usr.EndPlace = result.EndPlace;


        //    }
        //    return usr;

        //}



        [System.Web.Http.Route("api/TransportApi/deleteRouteById")]
        [System.Web.Http.HttpPost]
        public string deleteRouteById(List<string> id)
        {
            int idd = Convert.ToInt16(id[0]);
            var res = db.tblTransRoutes.SingleOrDefault(s => s.Id == idd);
            res.IsDeleted = 1;
            res.Deleted_on = DateTime.Now;
            
            //db.tblTransRoutes.Remove(res);
            db.SaveChanges();
            return "Route Deleted Successfully";


        }

        [System.Web.Http.Route("api/TransportApi/getAllRouteDetailsBySchool")]
        [System.Web.Http.HttpPost]
        public DepartmentMaster[] getAllRouteDetailsBySchool(List<string> a)
        {
            List<DepartmentMaster> list = new List<DepartmentMaster>();
            sqlHelper obj = new sqlHelper();
            int SchoolID = Convert.ToInt32(a[0]);
            DataTable dt = obj.getDataTable(@"SELECT * FROM tblTransRoute where SchoolID='"+ SchoolID + "' and IsDeleted is null");
            foreach (DataRow dr in dt.Rows)
            {
                DepartmentMaster usr = new DepartmentMaster();
                usr.Id = dr["Id"].ToString();
                usr.Name = dr["StartPlace"].ToString() + "-" + dr["EndPlace"].ToString() + " (" + dr["RouteCode"].ToString() + ")";


                list.Add(usr);
            }
            return list.ToArray();


        }


        [System.Web.Http.Route("api/TransportApi/getAllRouteDetails")]
        [System.Web.Http.HttpPost]
        public DepartmentMaster[] getAllRouteDetails()
        {
            List<DepartmentMaster> list = new List<DepartmentMaster>();
            sqlHelper obj = new sqlHelper();
            DataTable dt = obj.getDataTable(@"select * from tblTransRoute");
            foreach (DataRow dr in dt.Rows)
            {
                DepartmentMaster usr = new DepartmentMaster();
                usr.Id = dr["Id"].ToString();
                usr.Name = dr["StartPlace"].ToString() + "-" + dr["EndPlace"].ToString() + " (" + dr["RouteCode"].ToString() + ")";


                list.Add(usr);
            }
            return list.ToArray();


        }



        //[System.Web.Http.Route("api/TransportApi/saveDestinationDetails")]
        //[System.Web.Http.HttpPost]
        //public string saveDestinationDetails(DestinationDetails vd)
        //{
        //    if (vd.Id == 0)
        //    {
        //        tblRoteDestination tbl = new tblRoteDestination();
        //        tbl.RouteId = Convert.ToInt16(vd.Route);
        //        tbl.PickAndDrop = vd.Pickdrop;
        //        tbl.StopTime = vd.StopTime;
        //        tbl.DateCreated = DateTime.Now;
        //        db.tblRoteDestinations.Add(tbl);
        //        db.SaveChanges();
        //        return "Route Destination Inserted Successfully";
        //    }
        //    else
        //    {
        //        var result = db.tblRoteDestinations.Where(s => s.Id == vd.Id).SingleOrDefault();
        //        result.RouteId = Convert.ToInt16(vd.Route);
        //        result.PickAndDrop = vd.Pickdrop;
        //        result.StopTime = vd.StopTime;
        //        result.DateCreated = DateTime.Now;
        //        db.SaveChanges();
        //        return "Route Destination Updated Successfully";
        //    }


        //}

        [System.Web.Http.Route("api/TransportApi/ViewAllDestination")]
        [System.Web.Http.HttpPost]
        public DestinationDetails[] ViewAllDestination()
        {
            List<DestinationDetails> list = new List<DestinationDetails>();
            sqlHelper obj = new sqlHelper();
            DataTable dt = obj.getDataTable(@"select StartPlace+'-'+ EndPlace+' ('+RouteCode+')' RouteName,a.School,rd.* from tblRoteDestination rd
                                              inner join tblTransRoute r on r.Id=rd.RouteId left outer join tblSchoolDetails a on a.ID =rd.SchoolID where rd.IsDeleted is null");
            foreach (DataRow dr in dt.Rows)
            {
                DestinationDetails usr = new DestinationDetails();
                usr.Id = Convert.ToInt16(dr["Id"].ToString());
                usr.Route = dr["RouteName"].ToString();
                usr.Pickdrop = dr["PickAndDrop"].ToString();
                usr.StopTime = dr["StopTime"].ToString();
                usr.School = dr["School"].ToString();
                usr.SchoolID = dr["SchoolID"].ToString();

                list.Add(usr);
            }
            return list.ToArray();


        }

        //[System.Web.Http.Route("api/TransportApi/editRouteDestinationById")]
        //[System.Web.Http.HttpPost]
        //public DestinationDetails editRouteDestinationById(List<string> id)
        //{
        //    int idd = Convert.ToInt16(id[0]);
        //    var result = db.tblRoteDestinations.Where(s => s.Id == idd).SingleOrDefault();
        //    DestinationDetails usr = new DestinationDetails();
        //    if (result != null)
        //    {
        //        usr.Id = result.Id;
        //        usr.Route = Convert.ToString(result.RouteId);
        //        usr.Pickdrop = result.PickAndDrop;
        //        usr.StopTime = result.StopTime;
        //    }
        //    return usr;

        //}
        [System.Web.Http.Route("api/TransportApi/editRouteDestinationById")]
        [System.Web.Http.HttpPost]
        public DestinationDetails editRouteDestinationById(List<string> id)
        {

            int idd = Convert.ToInt16(id[0]);
            sqlHelper obj = new sqlHelper();
            DataTable dt = obj.getDataTable(@"select * from tblRoteDestination where Id='" + idd + "'");
            DestinationDetails usr = new DestinationDetails();
            foreach (DataRow dr in dt.Rows)
            {

                usr.Id = int.Parse(dr["Id"].ToString());
                usr.Route = dr["RouteId"].ToString();
                usr.Pickdrop = dr["PickAndDrop"].ToString();
                usr.StopTime = dr["StopTime"].ToString();


                usr.SchoolID = dr["SchoolID"].ToString();
            }
            return usr;
        }

        [System.Web.Http.Route("api/TransportApi/deleteRouteDestinationById")]
        [System.Web.Http.HttpPost]
        public string deleteRouteDestinationById(List<string> id)
        {
            int idd = Convert.ToInt16(id[0]);
            var res = db.tblRoteDestinations.SingleOrDefault(s => s.Id == idd);
            res.IsDeleted = 1;
            res.Deleted_on = DateTime.Now;
            db.SaveChanges();
            //db.tblRoteDestinations.Remove(res);
            //db.SaveChanges();
            return "Route Destination Deleted Successfully";


        }
    

        [System.Web.Http.Route("api/TransportApi/deleteVehicleByIddddd")]
        [System.Web.Http.HttpGet]
        public string deleteVehicleByIddddd(string Id)
        {
            bool b = LeaveManagement.deleteVehicleById(Id);
            if (b)
            {
                return "Vehicle Details  Deleted Successfully";
            }
            else
            {
                return "Vehicle Details not Deleted ";
            }

        }


        [System.Web.Http.Route("api/TransportApi/getDestinationByRouteIdBySchool")]
        [System.Web.Http.HttpPost]
        public DestinationDetails[] getDestinationByRouteIdBySchool(List<string> Id)
        {
            int idd = Convert.ToInt16(Id[0]);
            int SchoolID = Convert.ToInt32(Id[1]);
            List<DestinationDetails> list = new List<DestinationDetails>();
            sqlHelper obj = new sqlHelper();
            var result = db.tblRoteDestinations.Where(s => s.RouteId == idd && s.SchoolID== SchoolID && s.IsDeleted==null).ToList();
            foreach (var dr in result)
            {
                DestinationDetails usr = new DestinationDetails();
                usr.Id = dr.Id;
                usr.Route = dr.PickAndDrop;
                list.Add(usr);
            }
            return list.ToArray();


        }


        [System.Web.Http.Route("api/TransportApi/getDestinationByRouteId")]
        [System.Web.Http.HttpPost]
        public DestinationDetails[] getDestinationByRouteId(List<string> Id)
        {
            int idd = Convert.ToInt16(Id[0]);
            List<DestinationDetails> list = new List<DestinationDetails>();
            sqlHelper obj = new sqlHelper();
            var result = db.tblRoteDestinations.Where(s => s.RouteId == idd).ToList();
            foreach (var dr in result)
            {
                DestinationDetails usr = new DestinationDetails();
                usr.Id = dr.Id;
                usr.Route = dr.PickAndDrop;
                list.Add(usr);
            }
            return list.ToArray();


        }

        [System.Web.Http.Route("api/TransportApi/getAllStudentByClassSectionIdbySchool")]
        [System.Web.Http.HttpPost]
        public Student[] getAllStudentByClassSectionIdbySchool(Student std)
        {
            // int idd = Convert.ToInt16(Id[0]);

            List<Student> list = new List<Student>();
            int schoolid = Convert.ToInt32(std.SchoolID);
            var result = db.TBLStudents.Where(s => s.ClassID == std.ClassID && s.SectionID == std.SectionID && s.Status == 3 && s.IsDeleted==null && s.SchoolID== schoolid).ToList();
            foreach (var dr in result)
            {
                Student usr = new Student();
                usr.ID = dr.ID;
                usr.FirstName = dr.FirstName + " " + dr.MiddleName + " " + dr.LastName;
                list.Add(usr);
            }
            return list.ToArray();


        }


        [System.Web.Http.Route("api/TransportApi/getAllStudentByClassSectionId")]
        [System.Web.Http.HttpPost]
        public Student[] getAllStudentByClassSectionId(Student std)
        {
            // int idd = Convert.ToInt16(Id[0]);

            List<Student> list = new List<Student>();
            var result = db.TBLStudents.Where(s => s.ClassID == std.ClassID && s.SectionID == std.SectionID && s.Status == 3).ToList();
            foreach (var dr in result)
            {
                Student usr = new Student();
                usr.ID = dr.ID;
                usr.FirstName = dr.FirstName + " " + dr.MiddleName + " " + dr.LastName;
                list.Add(usr);
            }
            return list.ToArray();


        }


        [System.Web.Http.Route("api/TransportApi/getAllEmployeeByDepIdBySchool")]
        [System.Web.Http.HttpPost]
        public DestinationDetails[] getAllEmployeeByDepIdBySchool(List<string> Id)
        {
            int idd = Convert.ToInt16(Id[0]);
            int SchoolID = Convert.ToInt32(Id[1]);
            List<DestinationDetails> list = new List<DestinationDetails>();
            sqlHelper obj = new sqlHelper();
            var result = db.tblEmployees.Where(s => s.DeptID == idd && s.SchoolID== SchoolID && s.IsDeleted==null).ToList();
            foreach (var dr in result)
            {
                DestinationDetails usr = new DestinationDetails();
                usr.Id = dr.Id;
                usr.Route = dr.FirstName + " " + dr.MiddleName + " " + dr.LastName;
                list.Add(usr);
            }
            return list.ToArray();


        }

        [System.Web.Http.Route("api/TransportApi/getAllEmployeeByDepId")]
        [System.Web.Http.HttpPost]
        public DestinationDetails[] getAllEmployeeByDepId(List<string> Id)
        {
            int idd = Convert.ToInt16(Id[0]);
            List<DestinationDetails> list = new List<DestinationDetails>();
            sqlHelper obj = new sqlHelper();
            var result = db.tblEmployees.Where(s => s.DeptID == idd).ToList();
            foreach (var dr in result)
            {
                DestinationDetails usr = new DestinationDetails();
                usr.Id = dr.Id;
                usr.Route = dr.FirstName + " " + dr.MiddleName + " " + dr.LastName;
                list.Add(usr);
            }
            return list.ToArray();


        }

        [System.Web.Http.Route("api/TransportApi/saveTransportAllocationDetails")]
        [System.Web.Http.HttpPost]
        public string saveTransportAllocationDetails(TransportAllocation vd)
        {
            if (vd.UserType == "Student")
            {

                if (vd.Id == 0)
                {





                    foreach (var a in vd.stu.ToList())
                    {
                        var result = db.tblTransportAllocations.Where(s => s.StudentId == a.StudentName).ToList();
                        foreach (var f in result)
                        {

                            db.tblTransportAllocations.Remove(f);
                        }



                        tblTransportAllocation tbl = new tblTransportAllocation();
                        tbl.RouteId = vd.route;
                        tbl.DestinationId = vd.Destination;
                        tbl.UserType = vd.UserType;
                        tbl.ClassId = a.Class1;
                        tbl.SectionId = a.Section1;
                        tbl.StudentId = a.StudentName;
                        tbl.DepId = "0";
                        tbl.EmployeeId = "0";
                        tbl.DateCreated = DateTime.Now;
                        tbl.Status = true;
                        tbl.SchoolID = vd.SchoolID;
                        db.tblTransportAllocations.Add(tbl);
                        db.SaveChanges();


                    }
                    return "Transport Allocate Successfully";


                }

                else
                {
                    var tbl = db.tblTransportAllocations.Where(s => s.Id == vd.Id).SingleOrDefault();
                    tbl.RouteId = vd.route;
                    tbl.DestinationId = vd.Destination;
                    tbl.UserType = vd.UserType;
                    tbl.ClassId = vd.Class;
                    tbl.SectionId = vd.Section;
                    tbl.StudentId = vd.StudentName;
                    tbl.SchoolID = vd.SchoolID;
                    tbl.DepId = "0";
                    tbl.EmployeeId = "0";
                    tbl.DateCreated = DateTime.Now;
                    //    db.tblTransportAllocations.Add(tbl);
                    db.SaveChanges();
                    return "Transport Allocated Updated Successfully";


                }
            }
            else
            {
                if (vd.Id == 0)
                {
                    tblTransportAllocation tbl = new tblTransportAllocation();
                    tbl.RouteId = vd.route;
                    tbl.DestinationId = vd.Destination;
                    tbl.UserType = vd.UserType;
                    tbl.DepId = vd.Department;
                    tbl.EmployeeId = vd.EmployeeName;
                    tbl.DateCreated = DateTime.Now;
                    tbl.Status = true;
                    tbl.ClassId = "0";
                    tbl.SectionId = "0";
                    tbl.StudentId = "0";
                    tbl.SchoolID = vd.SchoolID;
                    db.tblTransportAllocations.Add(tbl);
                    db.SaveChanges();
                    return "Transport Allocate Successfully";

                }
                else
                {
                    var emptrans = db.tblTransportAllocations.SingleOrDefault(s => s.Id == vd.Id);
                    emptrans.RouteId = vd.route;
                    emptrans.DestinationId = vd.Destination;
                    emptrans.UserType = vd.UserType;
                    emptrans.DepId = vd.Department;
                    emptrans.EmployeeId = vd.EmployeeName;
                    emptrans.DateCreated = DateTime.Now;
                    emptrans.ClassId = "0";
                    emptrans.SectionId = "0";
                    emptrans.StudentId = "0";
                    emptrans.SchoolID = vd.SchoolID;
                    db.SaveChanges();
                    return "Transport Allocated Updated Successfully";
                }

            }
            return "";
        }


        [System.Web.Http.Route("api/TransportApi/getAllAllocationDetails")]
        [System.Web.Http.HttpPost]
        public TransportAllocation[] getAllAllocationDetails(List<string> aa)
        {
            int SchoolID = Convert.ToInt32(aa[0]);
            List<TransportAllocation> list = new List<TransportAllocation>();
            sqlHelper obj = new sqlHelper();
            DataTable dt = obj.getDataTable(@"select ea.Id,r.RouteCode,ea.UserType,e.Empcode ReisterNo,e.FirstName+' '+isnull(e.MiddleName,'')+' '+e.LastName   as Name,
                                        rd.PickAndDrop,rd.StopTime,ea.Status from  tblTransportAllocation ea
                                        inner join tblEmployee e on e.Id=ea.EmployeeId
                                        inner join tblTransRoute r on r.Id=ea.RouteId
                                        inner join tblRoteDestination rd on rd.RouteId=ea.RouteId
                                         where ea.SchoolID='"+SchoolID+"' and ea.UserType='Employee' union select ea.Id,r.RouteCode,ea.UserType,s.RegNo ReisterNo,s.FirstName+' '+isnull(s.MiddleName,'')+' '+s.LastName   as Name, rd.PickAndDrop,rd.StopTime,ea.Status from  tblTransportAllocation ea inner join TBLStudent s on s.Id=ea.StudentId inner join tblTransRoute r on r.Id=ea.RouteId inner join tblRoteDestination rd on rd.RouteId=ea.RouteId  where ea.schoolID='" + SchoolID + "'and ea.UserType='Student'  ");



            //select ea.Id,r.RouteCode,ea.UserType, e.Empcode ReisterNo, e.FirstName + ' ' + isnull(e.MiddleName, '') + ' ' + e.LastName as Name,
            //                            rd.PickAndDrop,rd.StopTime,ea.Status from  tblTransportAllocation ea
            //                            inner join tblEmployee e on e.Id = ea.EmployeeId
            //                            inner join tblTransRoute r on r.Id = ea.RouteId
            //                            inner join tblRoteDestination rd on rd.RouteId = ea.RouteId
            //                             where ea.SchoolID = 1 and ea.UserType = 'Employee'


            //                             union
            //                          select ea.Id,r.RouteCode,ea.UserType,s.RegNo ReisterNo, s.FirstName + ' ' + isnull(s.MiddleName, '') + ' ' + s.LastName as Name,
            //                            rd.PickAndDrop,rd.StopTime,ea.Status from  tblTransportAllocation ea
            //                            inner join TBLStudent s on s.Id = ea.StudentId
            //                            inner join tblTransRoute r on r.Id = ea.RouteId
            //                            inner join tblRoteDestination rd on rd.RouteId = ea.RouteId
            //                             where ea.UserType = 'Student'

            foreach (DataRow dr in dt.Rows)
            {
                TransportAllocation usr = new TransportAllocation();
                usr.Id = Convert.ToInt16(dr["Id"].ToString());
                usr.route = dr["RouteCode"].ToString();
                usr.Pickdrop = dr["PickAndDrop"].ToString();
                usr.StopTime = dr["StopTime"].ToString();

                usr.UserType = dr["UserType"].ToString();
                usr.StudentName = dr["Name"].ToString();
                usr.EmployeeName = dr["ReisterNo"].ToString();
                if (dr["Status"].ToString() == "True")
                {
                    usr.Status = "True";
                    usr.Extra10 = "btn btn-block btn-success btn-sm";
                }
                else
                {
                    usr.Status = "False";
                    usr.Extra10 = "btn btn-block btn-danger btn-sm";
                }

                list.Add(usr);
            }
            return list.ToArray();


        }

        [System.Web.Http.Route("api/TransportApi/editAllocationDetailsById")]
        [System.Web.Http.HttpPost]
        public TransportAllocation editAllocationDetailsById(Parameters param)
        {

            TransportAllocation usr = new TransportAllocation();
            if (param.val1 == "Student")
            {
                sqlHelper obj = new sqlHelper();
                SqlDataReader dr = obj.GetReader(@"select ea.Id,r.RouteCode,ea.UserType,s.RegNo ReisterNo,s.FirstName+' '+isnull(s.MiddleName,'')+' '+s.LastName   as Name,
                                            rd.PickAndDrop,rd.StopTime,ea.Status,c.CourseName Class,sec.SectionName from  tblTransportAllocation ea
                                            inner join TBLStudent s on s.Id=ea.StudentId
                                            inner join tblCourses c on c.Id=s.ClassID
                                            inner join tblSections sec on sec.Id=s.SectionID and sec.ClassId=s.ClassID
                                            inner join tblTransRoute r on r.Id=ea.RouteId
                                            inner join tblRoteDestination rd on rd.RouteId=ea.RouteId
                                             where UserType='Student' and ea.Id=11");

                if (dr.Read())
                {

                    usr.Id = Convert.ToInt16(dr["Id"].ToString());
                    usr.route = dr["RouteCode"].ToString();
                    usr.Pickdrop = dr["PickAndDrop"].ToString();
                    usr.StopTime = dr["StopTime"].ToString();
                    usr.Class = dr["Class"].ToString() + " (" + dr["SectionName"].ToString() + ")";
                    usr.UserType = dr["UserType"].ToString();
                    usr.StudentName = dr["Name"].ToString();
                    usr.EmployeeName = dr["ReisterNo"].ToString();
                    if (dr["Status"].ToString() == "True")
                    {
                        usr.Status = "Active";
                        usr.Extra10 = "btn btn-block btn-success btn-sm";
                    }
                    else
                    {
                        usr.Status = "In-Active";
                        usr.Extra10 = "btn btn-block btn-danger btn-sm";
                    }




                }


            }
            return usr;

        }

        [System.Web.Http.Route("api/TransportApi/sendSmsForAllTransportAllocationUsers")]
        [System.Web.Http.HttpPost]
        public string sendSmsForAllTransportAllocationUsers(Parameters param)
        {
            sqlHelper obj = new sqlHelper();
            if (param.val == "1")
            {
              
                DataTable dt = obj.getDataTable(@"select ea.Id,r.RouteCode,ea.UserType,e.Empcode ReisterNo,e.FirstName+' '+isnull(e.MiddleName,'')+' '+e.LastName   as Name,
                                        e.Mobile MobileNo,rd.PickAndDrop,rd.StopTime,ea.Status from  tblTransportAllocation ea
                                        inner join tblEmployee e on e.Id=ea.EmployeeId
                                        inner join tblTransRoute r on r.Id=ea.RouteId
                                        inner join tblRoteDestination rd on rd.RouteId=ea.RouteId
                                         where UserType='Employee'


                                         union 
                                         select ea.Id,r.RouteCode,ea.UserType,s.RegNo ReisterNo,s.FirstName+' '+isnull(s.MiddleName,'')+' '+s.LastName   as Name,
                                        s.SMSmobileNo MobileNo,rd.PickAndDrop,rd.StopTime,ea.Status from  tblTransportAllocation ea
                                        inner join TBLStudent s on s.Id=ea.StudentId
                                        inner join tblTransRoute r on r.Id=ea.RouteId
                                        inner join tblRoteDestination rd on rd.RouteId=ea.RouteId
                                         where UserType='Student'");
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["MobileNo"].ToString() != "")
                    {
                        //     string phoneno = obj.ExecuteScaler("select Mobile from tblEmployee where Id=" + emp.Employeecode);
                        string strUrl = "http://msg.msgclub.net/rest/services/sendSMS/sendGroupSms?AUTH_KEY=78978f2548b73f5edb1db725fcf65127&message=" + param.val1 + "&senderId=DEMOOS&routeId=1&mobileNos=" + dr["MobileNo"].ToString() + "&smsContentType=english";
                        // string strUrl = "http://api.mVaayoo.com/mvaayooapi/MessageCompose?user=YourUserName:YourPassword&senderID=YourSenderID&    receipientno=1234567890&msgtxt=This is a test from mVaayoo API&state=4";
                        // Create a request object  
                        WebRequest request = HttpWebRequest.Create(strUrl);
                        // Get the response back  
                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                        Stream s = (Stream)response.GetResponseStream();
                        StreamReader readStream = new StreamReader(s);
                        string dataString = readStream.ReadToEnd();
                        response.Close();
                        s.Close();
                        readStream.Close();
                    }
                }


            }
            else if (param.val == "2")
            {
                
                DataTable dt = obj.getDataTable(@"select ea.Id,r.RouteCode,ea.UserType,e.Empcode ReisterNo,e.FirstName+' '+isnull(e.MiddleName,'')+' '+e.LastName   as Name,
                                        e.Mobile MobileNo,rd.PickAndDrop,rd.StopTime,ea.Status from  tblTransportAllocation ea
                                        inner join tblEmployee e on e.Id=ea.EmployeeId
                                        inner join tblTransRoute r on r.Id=ea.RouteId
                                        inner join tblRoteDestination rd on rd.RouteId=ea.RouteId
                                         where UserType='Employee' and ea.RouteId=" + param.val2 + @"


                                         union 
                                         select ea.Id,r.RouteCode,ea.UserType,s.RegNo ReisterNo,s.FirstName+' '+isnull(s.MiddleName,'')+' '+s.LastName   as Name,
                                        s.SMSmobileNo MobileNo,rd.PickAndDrop,rd.StopTime,ea.Status from  tblTransportAllocation ea
                                        inner join TBLStudent s on s.Id=ea.StudentId
                                        inner join tblTransRoute r on r.Id=ea.RouteId
                                        inner join tblRoteDestination rd on rd.RouteId=ea.RouteId
                                         where UserType='Student' and ea.RouteId=" + param.val2);
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["MobileNo"].ToString() != "")
                    {
                        //     string phoneno = obj.ExecuteScaler("select Mobile from tblEmployee where Id=" + emp.Employeecode);
                        string strUrl = "http://msg.msgclub.net/rest/services/sendSMS/sendGroupSms?AUTH_KEY=78978f2548b73f5edb1db725fcf65127&message=" + param.val1 + "&senderId=DEMOOS&routeId=1&mobileNos=" + dr["MobileNo"].ToString() + "&smsContentType=english";
                        // string strUrl = "http://api.mVaayoo.com/mvaayooapi/MessageCompose?user=YourUserName:YourPassword&senderID=YourSenderID&    receipientno=1234567890&msgtxt=This is a test from mVaayoo API&state=4";
                        // Create a request object  
                        WebRequest request = HttpWebRequest.Create(strUrl);
                        // Get the response back  
                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                        Stream s = (Stream)response.GetResponseStream();
                        StreamReader readStream = new StreamReader(s);
                        string dataString = readStream.ReadToEnd();
                        response.Close();
                        s.Close();
                        readStream.Close();
                    }
                }
            }
            else
            {
                DataTable dt = obj.getDataTable(@"select ea.Id,r.RouteCode,ea.UserType,e.Empcode ReisterNo,e.FirstName+' '+isnull(e.MiddleName,'')+' '+e.LastName   as Name,
                                        e.Mobile MobileNo,rd.PickAndDrop,rd.StopTime,ea.Status from  tblTransportAllocation ea
                                        inner join tblEmployee e on e.Id=ea.EmployeeId
                                        inner join tblTransRoute r on r.Id=ea.RouteId
                                        inner join tblRoteDestination rd on rd.RouteId=ea.RouteId
                                         where UserType='Employee' and ea.RouteId=" + param.val2 +" and ea.DestinationId=" + param.val3 + @"


                                         union 
                                         select ea.Id,r.RouteCode,ea.UserType,s.RegNo ReisterNo,s.FirstName+' '+isnull(s.MiddleName,'')+' '+s.LastName   as Name,
                                        s.SMSmobileNo MobileNo,rd.PickAndDrop,rd.StopTime,ea.Status from  tblTransportAllocation ea
                                        inner join TBLStudent s on s.Id=ea.StudentId
                                        inner join tblTransRoute r on r.Id=ea.RouteId
                                        inner join tblRoteDestination rd on rd.RouteId=ea.RouteId
                                         where UserType='Student'and  ea.RouteId=" + param.val2 + " and ea.DestinationId=" + param.val3);
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["MobileNo"].ToString() != "")
                    {
                        //     string phoneno = obj.ExecuteScaler("select Mobile from tblEmployee where Id=" + emp.Employeecode);
                        string strUrl = "http://msg.msgclub.net/rest/services/sendSMS/sendGroupSms?AUTH_KEY=78978f2548b73f5edb1db725fcf65127&message=" + param.val1 + "&senderId=DEMOOS&routeId=1&mobileNos=" + dr["MobileNo"].ToString() + "&smsContentType=english";
                        // string strUrl = "http://api.mVaayoo.com/mvaayooapi/MessageCompose?user=YourUserName:YourPassword&senderID=YourSenderID&    receipientno=1234567890&msgtxt=This is a test from mVaayoo API&state=4";
                        // Create a request object  
                        WebRequest request = HttpWebRequest.Create(strUrl);
                        // Get the response back  
                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                        Stream s = (Stream)response.GetResponseStream();
                        StreamReader readStream = new StreamReader(s);
                        string dataString = readStream.ReadToEnd();
                        response.Close();
                        s.Close();
                        readStream.Close();
                    }
                }
            }
            return "SMS Send Successfully Thank u!";

            //saveTransportAllocationDetails
        }


        [System.Web.Http.Route("api/TransportApi/getAllStudentByClassSectionIdForChangeTransportBySchool")]
        [System.Web.Http.HttpPost]
        public TransportAllocation[] getAllStudentByClassSectionIdForChangeTransportBySchool(Student std)
        {

            List<TransportAllocation> list = new List<TransportAllocation>();

            sqlHelper obj = new sqlHelper();
            DataTable dt = obj.getDataTable(@"select ea.StudentId,s.FirstName+' '+isnull(s.MiddleName,'')+' '+s.LastName   as Name
                                            from  tblTransportAllocation ea
                                            inner join TBLStudent s on s.Id=ea.StudentId and s.ClassID=" + std.ClassID + " and s.SectionID=" + std.SectionID + @"
                                            where UserType='Student' and ea.Status=1 and  ea.SchoolID='"+std.SchoolID+"'");

            foreach (DataRow dr in dt.Rows)
            {
                TransportAllocation usr = new TransportAllocation();

                usr.Id = Convert.ToInt16(dr["StudentId"].ToString());

                usr.StudentName = dr["Name"].ToString();

                list.Add(usr);



            }



            return list.ToArray();

        }



        [System.Web.Http.Route("api/TransportApi/getAllStudentByClassSectionIdForChangeTransport")]
        [System.Web.Http.HttpPost]
        public TransportAllocation[] getAllStudentByClassSectionIdForChangeTransport(Student std)
        {

            List<TransportAllocation> list = new List<TransportAllocation>(); 
         
                sqlHelper obj = new sqlHelper();
                DataTable dt = obj.getDataTable(@"select ea.StudentId,s.FirstName+' '+isnull(s.MiddleName,'')+' '+s.LastName   as Name
                                            from  tblTransportAllocation ea
                                            inner join TBLStudent s on s.Id=ea.StudentId and s.ClassID="+std.ClassID +" and s.SectionID="+std.SectionID+@"
                                            where UserType='Student' and ea.Status=1");

              foreach(DataRow dr in dt.Rows)
                {
                    TransportAllocation usr = new TransportAllocation();

                    usr.Id = Convert.ToInt16(dr["StudentId"].ToString());
                  
                    usr.StudentName = dr["Name"].ToString();

                    list.Add(usr);



                }


             
            return list.ToArray() ;

        }

        [System.Web.Http.Route("api/TransportApi/getDesignationFromStaudnetbySchool")]
        [System.Web.Http.HttpPost]
        public TransportAllocation[] getDesignationFromStaudnetbySchool(Student std)
        {

            List<TransportAllocation> list = new List<TransportAllocation>();

            sqlHelper obj = new sqlHelper();
            DataTable dt = obj.getDataTable(@"select rd.Id,
                                         rd.PickAndDrop,rd.StopTime from  tblTransportAllocation ea
                                         inner join tblRoteDestination rd on rd.RouteId=ea.RouteId
                                        where UserType='Student' and ea.StudentId=" + std.SectionID + " and ea.SchoolID='"+std.SchoolID+"' and ea.IsDeleted is null and ea.Status=1 ");

            foreach (DataRow dr in dt.Rows)
            {
                TransportAllocation usr = new TransportAllocation();

                usr.Id = Convert.ToInt16(dr["Id"].ToString());

                usr.StudentName = dr["PickAndDrop"].ToString();

                list.Add(usr);



            }



            return list.ToArray();

        }



        [System.Web.Http.Route("api/TransportApi/getDesignationFromStaudnet")]
        [System.Web.Http.HttpPost]
        public TransportAllocation[] getDesignationFromStaudnet(Student std)
        {

            List<TransportAllocation> list = new List<TransportAllocation>();

            sqlHelper obj = new sqlHelper();
            DataTable dt = obj.getDataTable(@"select rd.Id,
                                         rd.PickAndDrop,rd.StopTime from  tblTransportAllocation ea
                                         inner join tblRoteDestination rd on rd.RouteId=ea.RouteId
                                        where UserType='Student' and ea.StudentId="+std.SectionID+"  and ea.Status=1 ");

            foreach (DataRow dr in dt.Rows)
            {
                TransportAllocation usr = new TransportAllocation();

                usr.Id = Convert.ToInt16(dr["Id"].ToString());

                usr.StudentName = dr["PickAndDrop"].ToString();

                list.Add(usr);



            }



            return list.ToArray();

        }




        [System.Web.Http.Route("api/TransportApi/getAllEmployeeByDepIdChangeTransport")]
        [System.Web.Http.HttpPost]
        public TransportAllocation[] getAllEmployeeByDepIdChangeTransport(List<string> Id)
        {
            int idd = Convert.ToInt16(Id[0]);
            List<TransportAllocation> list = new List<TransportAllocation>();

            sqlHelper obj = new sqlHelper();
            DataTable dt = obj.getDataTable(@"select e.Id,e.FirstName+' '+isnull(e.MiddleName,'')+' '+e.LastName   as Name
                                                    from  tblTransportAllocation ea
                                                    inner join tblEmployee e on e.Id=ea.EmployeeId and e.DeptID="+idd+@"
                                                              where ea.UserType='Employee'  and ea.Status=1");

            foreach (DataRow dr in dt.Rows)
            {
                TransportAllocation usr = new TransportAllocation();

                usr.Id = Convert.ToInt16(dr["Id"].ToString());

                usr.EmployeeName = dr["Name"].ToString();

                list.Add(usr);



            }
            return list.ToArray();
        }
        [System.Web.Http.Route("api/TransportApi/getDesignationFromEmployeebySchool")]
        [System.Web.Http.HttpPost]
        public TransportAllocation[] getDesignationFromEmployeebySchool(Student std)
        {

            List<TransportAllocation> list = new List<TransportAllocation>();

            sqlHelper obj = new sqlHelper();
            DataTable dt = obj.getDataTable(@"select  rd.Id,rd.PickAndDrop,rd.StopTime,ea.Status from  tblTransportAllocation ea
                                              inner join tblRoteDestination rd on rd.RouteId=ea.RouteId
                                             where UserType='Employee' and ea.EmployeeId=" + std.SectionID + " and ea.Status=1 and ea.SchoolID='"+std.SchoolID+ "' and ea.IsDeleted is null  ");

            foreach (DataRow dr in dt.Rows)
            {
                TransportAllocation usr = new TransportAllocation();

                usr.Id = Convert.ToInt16(dr["Id"].ToString());

                usr.StudentName = dr["PickAndDrop"].ToString();

                list.Add(usr);



            }



            return list.ToArray();

        }

        [System.Web.Http.Route("api/TransportApi/getDesignationFromEmployee")]
        [System.Web.Http.HttpPost]
        public TransportAllocation[] getDesignationFromEmployee(Student std)
        {

            List<TransportAllocation> list = new List<TransportAllocation>();

            sqlHelper obj = new sqlHelper();
            DataTable dt = obj.getDataTable(@"select  rd.Id,rd.PickAndDrop,rd.StopTime,ea.Status from  tblTransportAllocation ea
                                              inner join tblRoteDestination rd on rd.RouteId=ea.RouteId
                                             where UserType='Employee' and ea.EmployeeId="+std.SectionID+" and ea.Status=1 ");

            foreach (DataRow dr in dt.Rows)
            {
                TransportAllocation usr = new TransportAllocation();

                usr.Id = Convert.ToInt16(dr["Id"].ToString());

                usr.StudentName = dr["PickAndDrop"].ToString();

                list.Add(usr);



            }



            return list.ToArray();

        }

        [System.Web.Http.Route("api/TransportApi/changeTransportRouteDestinationBySchool")]
        [System.Web.Http.HttpPost]
        public string changeTransportRouteDestinationBySchool(TransportAllocation vd)
        {
            if (vd.UserType == "Student")
            {
                var result = db.tblTransportAllocations.SingleOrDefault(s => s.StudentId == vd.EmployeeName && s.SchoolID==vd.SchoolID);
                result.RouteId = vd.route;
                result.DestinationId = vd.Destination;
                result.SchoolID = vd.SchoolID;
                db.SaveChanges();
                return "Route Destinage Allocated Successfully";
            }
            else
            {
                var result = db.tblTransportAllocations.SingleOrDefault(s => s.EmployeeId == vd.EmployeeName && s.SchoolID == vd.SchoolID);
                result.RouteId = vd.route;
                result.DestinationId = vd.Destination;
                result.SchoolID = vd.SchoolID;
                db.SaveChanges();
                return "Route Destinage Allocated Successfully";
            }

        }



        [System.Web.Http.Route("api/TransportApi/changeTransportRouteDestination")]
        [System.Web.Http.HttpPost]
        public string changeTransportRouteDestination(TransportAllocation vd)
        {
            if (vd.UserType == "Student")
            {
                var result = db.tblTransportAllocations.SingleOrDefault(s => s.StudentId == vd.EmployeeName);
                result.RouteId = vd.route;
                result.DestinationId = vd.Destination;
                db.SaveChanges();
                return "Route Destinage Allocated Successfully";
            }
            else {
                var result = db.tblTransportAllocations.SingleOrDefault(s => s.EmployeeId == vd.EmployeeName);
                result.RouteId = vd.route;
                result.DestinationId = vd.Destination;
                db.SaveChanges();
                return "Route Destinage Allocated Successfully";
            }

        }

                

    }
}
